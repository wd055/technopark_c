#include "my_struct.h"

void print_struct(struct_t elem) {
    printf("id: %d, score: %f, score count: %d\n",
        elem.id, elem.score, elem.score_count);
}

void print_structs(struct_t elems[], size_t len) {
    for (size_t i = 0; i < len; i++) {
        print_struct(elems[i]);
    }
}

int read_struct_stream(FILE *stream, struct_t *elem) {
    if (fscanf(stream, "%d", &elem->id) != 1)
        return STRUCT_READ_ERROR;

    if (fscanf(stream, "%f", &elem->score) != 1)
        return STRUCT_READ_ERROR;

    if (fscanf(stream, "%d", &elem->score_count) != 1)
        return STRUCT_READ_ERROR;

    return 0;
}
