#include "dinamic.h"

long get_zero_count_sync(struct_t *arr, size_t len, size_t process_count) {
    int err = 0, stat;
    long count = 0;

    size_t page_size = getpagesize();
    long *shared_memory = (long*)mmap(NULL, page_size, PROT_READ | PROT_WRITE,
                               MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    if (!shared_memory) {
        return 0;
    }

    *shared_memory = 0;
    pid_t pid[process_count];
    size_t pid_i;
    for (pid_i = 0; pid_i < process_count; pid_i++) {
        long tmp_len = len / process_count;
        if (pid_i + 1 == process_count)
            tmp_len += len % process_count;

        pid[pid_i] = fork();
        if (pid[pid_i] == -1) {
            printf("Fork failed\n");
            if (munmap(shared_memory, page_size)) {
                printf("Failed to unmap\n");
            }
            err = ALLOC_ERR;
        } else if (pid[pid_i] == 0) {
            // count = get_zero_count_async(arr, tmp_len);
            // printf("pid_i: %lld, tmp_len: %ld, count: %ld\n", pid_i, tmp_len, count);
            *shared_memory += get_zero_count_async(arr, tmp_len);
            exit(0);
            break;
        }
        arr += tmp_len;
    }
    for (size_t i = 0; i < process_count; i++)
        waitpid(pid[i], &stat, 0);

    count = *shared_memory;

    if (pid && !err) {
        if (munmap(shared_memory, page_size)) {
            printf("Failed to unmap\n");
        }
    }

    if (err)
        return 0;
    else
        return count;
}