#include "work.h"

int main(int argc, char **argv)
{
    setbuf(stdout, NULL);
    int err = 0, print_structs_flag = FALSE, speed_test_flag = FALSE, speed_test_only_flag = FALSE;
    long speed_test_len = 0;
    int process_count = 1;
    long count = 0;
    char *file_name = "";

    int opt, opt_idx;
    struct option options[] = {
        {"help", no_argument, NULL, 'h'},
        {"process", optional_argument, NULL, 'p'},
        {"file", optional_argument, NULL, 'f'},
        {"struct_print", optional_argument, NULL, 's'},
        {"test_speed", optional_argument, NULL, 't'},
        {"test_speed_only", optional_argument, NULL, 'o'},
        {NULL, 0, NULL, 0}};
    while ((opt = getopt_long(argc, argv, "f:p:st:o", options, &opt_idx)) != -1)
    {
        switch (opt)
        {
        case 'f':
            file_name = optarg;
            break;
        case 's':
            print_structs_flag = TRUE;
            break;
        case 'o':
            speed_test_only_flag = TRUE;
            break;
        case 't':
            speed_test_flag = TRUE;
            speed_test_len = optarg ? atoi(optarg) : 1;
            break;
        case 'p':
            process_count = optarg ? atoi(optarg) : 1;
            break;
        default:
            break;
        }
    }

    if (speed_test_flag)
        speed_test(speed_test_len, process_count);

    if (!speed_test_flag)
    {
        struct_t *ratings = calloc(DEFULT_ARR_LEN, sizeof(struct_t));
        size_t len = 0, buf_len = DEFULT_ARR_LEN;
        err = read_file(file_name, &ratings, &len, &buf_len);
        if (!err)
        {
            if (print_structs_flag)
                print_structs(ratings, len);
            printf("All count: %lu\n", len);

            count = get_zero_count(ratings, len, process_count);
            printf("Zero count: %ld\n", count);
        }
        free(ratings);
    }

    if (err)
        printf("ERR: %d\n", err);

    return err;
}