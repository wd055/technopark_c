#include "my_array.h"

int realloc_by_len(void **arr, size_t elem_size, size_t arr_len, size_t *buf_len) {
    if (arr_len < *buf_len)
        return EXIT_SUCCESS;

    void *tmp_arr = realloc(*arr, 2 * (*buf_len) * elem_size);
    if (tmp_arr) {
        *arr = tmp_arr;
        *buf_len *= 2;
    } else {
        return ALLOC_ERR;
    }
    return EXIT_SUCCESS;
}

int insert_to_array(void **arr, void *elem, size_t elem_size, size_t arr_len, size_t *buf_len) {
    int err = 0;
    err = realloc_by_len(arr, elem_size, arr_len, buf_len);
    if (err)
        return err;

    void *arr_i = (char *)*arr + arr_len * elem_size;
    memcpy(arr_i, elem, elem_size);
    return 0;
}