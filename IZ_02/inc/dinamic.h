#ifndef INC_DINAMIC_H_
#define INC_DINAMIC_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <getopt.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "work.h"

long get_zero_count_sync(struct_t *arr, size_t len, size_t process_count);

#endif  // INC_DINAMIC_H_
